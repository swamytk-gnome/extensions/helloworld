/* extension.js
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

/* Import all necessary modules */
const St = imports.gi.St;
const Clutter = imports.gi.Clutter;
const Main = imports.ui.main;

let panelButton;
let panelButtonText;

class Extension {

    constructor() {
	}

    enable() {

		/* Creating button */
		panelButton = new St.Button({
	            style_class : "panel-button",
	    });
	
		/* Creating label */
		panelButtonText = new St.Label({
	    	text : "Hello World!",
		    y_align : Clutter.ActorAlign.CENTER
	    });

		panelButton.add_actor(panelButtonText);

		/* Attaching event handlers to buttons */
		panelButton.connect('clicked', () => myEventHandler());
	
		Main.panel._leftBox.insert_child_at_index(panelButton, -1);
   	}

    disable() {
		Main.panel._leftBox.remove_child(panelButton);
    }
}

function init() {

    return new Extension();
}


function myEventHandler()
{
	panelButtonText.set_text("Hi Clicked!");
}


